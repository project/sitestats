
-- SUMMARY --

This module will check the different statistics of a site.
Google Pagerank
Alexa Traffic Rank
Google Backlink
Google Indexed
Alltheweb Indexed
DMOZ.org listing
Domain Age

You have to select the different statistics that you want to see. By going to www.example.com/admin/settings/sitestats

You can use this tool at www.example.com/sitestats .

---------
TODO: Fetch more such statistics :)
