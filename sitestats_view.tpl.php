<?php
$url = check_plain($rows['url']);
$path = drupal_get_path('module', 'sitestats').'/pr/pr-';
$to_fetch = $rows['to_fetch'];

if ($to_fetch['PR']) {
  $r[] = array('Google Pagerank for http://'.$url,  '<img src="'.$path.$rows['pr_1'].'.gif" />');
}

if ($to_fetch['AP']) {
  $r[] = array('Alexa Traffic Rank', $rows['alexa_rank'] !='' ? $rows['alexa_rank']: 'Not Available');
}

if ($to_fetch['GB']) {
  $r[] = array('Google Backlink ', $rows['google_backlink']);
}

if ($to_fetch['GI']) {
  $r[] = array('Google Indexed', $rows['google_indexed']);
}

if ($to_fetch['AI']) {
  $r[] = array('Alltheweb Indexed', $rows['alltheweb']);
}

if ($to_fetch['DM']) {
  $r[] = array('Listed in DMOZ.org?', $rows['dmoz']);
}

if ($to_fetch['DA']) {
  $r[] = array('Domain Age', $rows['age']);
}

if (variable_get('sitestats_mail_default', TRUE)) {
  $r[] = array(array('data' => $rows['register'], 'colspan' => '2'));
}

$v = theme('table', array(), $r);

$fieldset = array(
    '#title' => check_plain($url),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#value' => $v,
    );

echo theme('fieldset', $fieldset);
?>

