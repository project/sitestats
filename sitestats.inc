<?php


//--> for google pagerank
function _sitestats_strtonum($Str, $Check, $Magic) {
  $Int32Unit = 4294967296;  // 2^32

  $length = strlen($Str);
  for ($i = 0; $i < $length; $i++) {
    $Check *= $Magic;     
    //If the float is beyond the boundaries of integer (usually +/- 2.15e+9 = 2^31),
    //  the result of converting to integer is undefined
    //  refer to http://www.php.net/manual/en/language.types.integer.php
    if ($Check >= $Int32Unit) {
      $Check = ($Check - $Int32Unit * (int) ($Check / $Int32Unit));
      //if the check less than -2^31
      $Check = ($Check < -2147483648) ? ($Check + $Int32Unit) : $Check;
    }
    $Check += ord($Str{$i});
  }
  return $Check;
}

//--> for google pagerank
/*
 * Generate a hash for a url
 */
function _sitestats_hashurl($String)
{
  $Check1 = _sitestats_strtonum($String, 0x1505, 0x21);
  $Check2 = _sitestats_strtonum($String, 0, 0x1003F);

  $Check1 >>= 2;     
  $Check1 = (($Check1 >> 4) & 0x3FFFFC0 ) | ($Check1 & 0x3F);
  $Check1 = (($Check1 >> 4) & 0x3FFC00 ) | ($Check1 & 0x3FF);
  $Check1 = (($Check1 >> 4) & 0x3C000 ) | ($Check1 & 0x3FFF);   

  $T1 = (((($Check1 & 0x3C0) << 4) | ($Check1 & 0x3C)) <<2 ) | ($Check2 & 0xF0F );
  $T2 = (((($Check1 & 0xFFFFC000) << 4) | ($Check1 & 0x3C00)) << 0xA) | ($Check2 & 0xF0F0000 );

  return ($T1 | $T2);
}

//--> for google pagerank
/*
 * genearate a checksum for the hash string
 */
function _sitestats_checkhash($Hashnum) {
  $CheckByte = 0;
  $Flag = 0;

  $HashStr = sprintf('%u', $Hashnum);
  $length = strlen($HashStr);

  for ($i = $length - 1;  $i >= 0;  $i --) {
    $Re = $HashStr{$i};
    if (1 === ($Flag % 2)) {             
      $Re += $Re;     
      $Re = (int)($Re / 10) + ($Re % 10);
    }
    $CheckByte += $Re;
    $Flag ++;   
  }

  $CheckByte %= 10;
  if (0 !== $CheckByte) {
    $CheckByte = 10 - $CheckByte;
    if (1 === ($Flag % 2) ) {
      if (1 === ($CheckByte % 2)) {
        $CheckByte += 9;
      }
      $CheckByte >>= 1;
    }
  }

  return '7'.$CheckByte.$HashStr;
}

//get google pagerank
function _sitestats_getpagerank($url) {
  $query = "http://toolbarqueries.google.com/search?client=navclient-auto&ch=" . _sitestats_checkhash(_sitestats_hashurl($url)) . "&features=Rank&q=info:".$url . "&num=100&filter=0";
  $data = _sitestats_getpage($query);
  $pos = strpos($data, "Rank_");
  if ($pos === FALSE) {
    return '';
  } 
  else {
    $pagerank = (int) substr($data, $pos + 9);
    return $pagerank;
  }
}

//get alexa popularity
function _sitestats_get_alexa_popularity($url) {   
  $alexaxml = "http://xml.alexa.com/data?cli=10&dat=nsa&url=" . $url;

  $xml_parser = xml_parser_create();
  $data = _sitestats_getpage($alexaxml);
  xml_parse_into_struct($xml_parser, $data, $vals, $index);
  xml_parser_free($xml_parser);

  $index_popularity = $index['POPULARITY'][0];
  $alexarank = $vals[$index_popularity]['attributes']['TEXT'];
  return $alexarank;
}

//get google backlink
function _sitestats_google_backlink($uri) {
  $url = 'http://www.google.com/search?hl=en&lr=&ie=UTF-8&q=link:'.$uri.'&filter=0';
  $v = _sitestats_getpage($url);
  preg_match('/of about \<b\>(.*?)\<\/b\>/si',$v,$r);
  preg_match('/of \<b\>(.*?)\<\/b\>/si',$v,$s);
  if ($s[1] != 0) {
    return $s[1];
  } 
  else {
    return ($r[1]) ? $r[1] : '0';
  }
}


//get alltheweb search result count
function _sitestats_alltheweb_link($sURL) {
  $url = "http://www.alltheweb.com/search?cat=web&cs=utf-8&q=link%3A" . drupal_urlencode($sURL) . "&_sb_lang=any";
  $data = _sitestats_getpage($url);
  $spl = explode("</span> of <span class=\"ofSoMany\">", $data);
  $spl2 = explode("</span>", $spl[1]);
  $ret = trim($spl2[0]);
  if (strlen($ret) == 0)	{
    return 0;
  }
  else {
    return $ret;
  }
}

//get google indexed page
function _sitestats_google_indexed($uri) {
  $url = 'http://www.google.com/search?hl=en&lr=&ie=UTF-8&q=site:' . $uri . '&filter=0';
  $v = _sitestats_getpage($url);
  preg_match('/of about \<b\>(.*?)\<\/b\>/si', $v, $r);
  preg_match('/of \<b\>(.*?)\<\/b\>/si', $v, $s);
  if ($s[1] != 0) {
    return $s[1];
  } 
  else {
    return ($r[1]) ? $r[1] : '0';
  }
}

//function to check whether an url is listed in DMOZ(ODP), return 1 or 0
function _sitestats_dmoz_listed($url) {
  $dmozurl = 'http://search.dmoz.org/cgi-bin/search?search=' . $url;
  $data = _sitestats_getpage($dmozurl);
  $pos = strpos($data, 'match');
  if ($pos == 0) {
    return 'Not Listed';
  } 
  else {
    return 'Yes';
  }
}

function _sitestats_getage ($url) {
  $url = "http://www.who.is/whois/$url";
  $data = _sitestats_getpage($url);
  preg_match('#Creation Date: ([a-z0-9-]+)#si', $data, $p);
  if ($p[1]) {
    $time = time() - strtotime($p[1]);
    $value = format_interval($time);
  } else {
    $value = 'Unknown';
  }
  return $value;
}

function _sitestats_getpage($request_url) {
  $answer = drupal_http_request($request_url);
  if ($answer->code == '200') {
    return $answer->data;
  }
  else {
    return '';
  }
}


?>
