
function sitestats_handler(event) {

  $.ajax({
    url:      'sitestats/path',
    data:     "url="+$('textarea[name="url"]').val(),
    type:     'POST',
    dataType: 'html',
    beforeSend: function(){
      $('#target_sitestats').empty();
      $('#loading1').show();
    },
    success: function(response) {
      $('#target_sitestats').html(response);
      $('#loading1').hide();
      $("input[@name=email]").keyup(sitestats_validate);
      $("input[@name=register]").click(sitestats_register);
      Drupal.attachBehaviors();
      }
  });
  return false;
}

function sitestats_validate(event) {
  var email = $(this).val();
  if (email != 0) {
    if (sitestats_isvalidemailaddress(email)) {
      $(this).parent().find("#validEmail").addClass('validYes').removeClass('validNo');
    }
    else {
      $(this).parent().find("#validEmail").addClass('validNo').removeClass('validYes');
    }
  }
  else {
    $(this).parent().find("#validEmail").removeClass('validNo').removeClass('validYes');
  }
}

function sitestats_register(event) {
  var $name = $(this).parent();
  var $value = $(this).parents().find('input[name="email"]').val();
  var $url = $(this).parents().find('input[name="url"]').val();
  if ( !sitestats_isvalidemailaddress($(this).parents().find('input[name="email"]').val())){
    alert('Please Enter a valid Email Address');
    return false;
  }

  $.ajax({
    url:      'sitestats/mail',
    data:     "url="+$url+
              "&email="+$value,
    type:     'POST',
    dataType: 'text',
    beforeSend: function(){
      $name.html('<div id="loading2"> </div>');
    },
    success: function(data) {
      $name.html(data);
    }
  });
  return false;
}

function sitestats_isvalidemailaddress(emailAddress) {
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  return pattern.test(emailAddress);
}

Drupal.behaviors.sitestats = function () {
    $("input[@name=submit]").click(sitestats_handler);
    $('#loading1').hide();
};

